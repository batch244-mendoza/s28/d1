// CRUD Operations
// C - reate, R - etrieve , U - pdate, D - elete 
// CRUD operations are the heart of any backend application

// [SECTION] Inserting documents (Create)

	// Syntax - db.collectionName.insertOne({object});

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21, 
	contact: {
		phone: "987654321",
		email:"janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript","Python"],
	department: "none"
});


// Inserting Many documents
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "987654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact:{
			phone: "987654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Sass", "Laravel"],
		department: "none"
	}
]);

db.users.insertMyDetails({
	firstName: "Arn",
	lastName: "Mendoza",
	age: 25, 
	contact: {
		phone: "987654321",
		email:"arnmendoza@gmail.com"
	},
	courses: ["PHP", "Laravel","Ruby on Rails"],
	department: "none"
});


// [SECTION] Finding documents (Read/Retrieve)
// Syntax: db.collectionName.find()
// Will retrieve all the documents
db.users.find();

// Finding documents with one field/parameter
db.users.find({firstName: "Stephen"});

// Finding documents with multiple parameters
db.users.find({lastName: "Armstrong", age: 82});


// [SECTION] Updating documents (Update)

// Creating a document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0, 
	contact: {
		phone: "00000000000",
		email:"test@gmail.com"
	},
	courses: [],
	department: "none"

});


// Updating a single document
/*
		Syntax:
		db.collectionName.updateOne({criteria},
		{$set: {field: value}});
	
*/ 

db.users.updateOne({firstName: "Test"}, 
	{	
		$set : {
			firstName: "Bill",
			lastName: "Gates",
			age: 65, 
			contact: {
				phone: "123456789",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

// Updating multipe documents
db.users.updateMany({department: "none"},
	{
		$set: {department: "HR"}
	}

	);

db.users.replaceOne({firstName: "Bill"},
		{
			firstName: "Test",
			lastName: "Test",
			age: 0, 
			contact: {
				phone: "123456789"
				email: "testgmail.com"
			},
			courses: [],
			
		}

	);

// [SECTION] Deleting documents (Delete)
// Deleting single/mutiple
// Syntax: db.collectionName.deleteOne({criteria});

db.users.deleteOne({firstName: "test"});

// Advanced Queries

// Query an embedded document
db.users.find({
		contact:{
			phone: "987654321",
			email: "stephenhawking@gmail.com"
		}	
});

// Query on nested field
db.users.find({
	"contact.email": "janedoe@gmail.com"
});

// Querying an array without regard to order
db.users.find({courses: {$all: ["React", "Python", ]}});

// Querying an array with exact elements
db.users.find({courses: ["CSS", "Javascript", "Python"]});

// Qeurying an embedded array
db.users.insert({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});

db.users.find({
	namearr: 
	{
		namea: "juan"
	}
});	